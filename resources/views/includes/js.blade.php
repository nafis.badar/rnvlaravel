<!-- jQuery -->
<script src="{!! asset('public/assets/vendor/jquery/jquery.min.js') !!}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{!! asset('public/assets/vendor/bootstrap/js/bootstrap.min.js') !!}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{!! asset('public/assets/vendor/metisMenu/metisMenu.min.js') !!}"></script>

<!-- DataTables JavaScript -->
<script src="{!! asset('public/assets/vendor/datatables/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('public/assets/vendor/datatables-plugins/dataTables.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/assets/vendor/datatables-responsive/dataTables.responsive.js') !!}"></script>

<!-- Custom Theme JavaScript -->
<script src="{!! asset('public/assets/dist/js/sb-admin-2.js') !!}"></script>


<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true,
            "aaSorting": []
        });
    });
</script>

<!-- Notification -->
<script src="{{ asset('public/assets/js/sweet-alert.js') }}"></script>
<script src="{!! asset('public/assets/js/bootstrap-notify.js') !!}"></script>