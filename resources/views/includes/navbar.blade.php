<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{!! URL('/') !!}">{!! Auth::user()->name !!}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="login.html" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href="{!!  URL('/home') !!}"><i class="fa fa-dashboard fa-fw"></i> Dashboard </a>
                        </li>
                        
                        <li>
                            <a href="{!! route('student.index') !!}"> <i class="fa fa-table fa-fw"></i> Students </a>
                        </li>
                        <li>
                            <a href="{!! route('staff.index') !!}"> <i class="fa fa-edit fa-fw"></i> Staffs </a>
                        </li>

                        <li>
                            <a href="{!! route('classtable.index') !!}"> <i class="fa fa-edit fa-fw"></i> Classes </a>
                        </li>

                        <li>
                            <a href="{!! route('section.index') !!}"> <i class="fa fa-edit fa-fw"></i> Sections </a>
                        </li>

                        <li>
                            <a href="{!! route('shift.index') !!}"> <i class="fa fa-edit fa-fw"></i> Shifts </a>
                        </li>

                        <li>
                            <a href="{!! route('subject.index') !!}"> <i class="fa fa-edit fa-fw"></i> Subjects </a>
                        </li>

                        <li>
                            <a href="{!! route('timeschedule.index') !!}"> <i class="fa fa-edit fa-fw"></i> Timeschedules </a>
                        </li>

                        <li>
                            <a href="{!! route('sectionschedule.index') !!}"> <i class="fa fa-edit fa-fw"></i> Section Schedules </a>
                        </li>

                        <li>
                            <a href="{!! route('staffattendance.index') !!}"> <i class="fa fa-edit fa-fw"></i> Staff Attendances </a>
                        </li>

                        <li>
                            <a href="{!! route('exam.index') !!}"> <i class="fa fa-edit fa-fw"></i> Exams </a>
                        </li>

                        <li>
                            <a href="{!! route('settings.edit') !!}"> <i class="fa fa-edit fa-fw"></i> Settings </a>
                        </li>
                        
                        
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>