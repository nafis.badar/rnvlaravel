<!-- Bootstrap Core CSS -->
<link href="{!! asset('public/assets/vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="{!! asset('public/assets/vendor/metisMenu/metisMenu.min.css') !!}" rel="stylesheet">

<!-- DataTables CSS -->
<link href="{!! asset('public/assets/vendor/datatables-plugins/dataTables.bootstrap.css') !!}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{!! asset('public/assets/vendor/datatables-responsive/dataTables.responsive.css') !!}" rel="stylesheet">

<!-- Custom CSS -->
<link href="{!! asset('public/assets/dist/css/sb-admin-2.css') !!}" rel="stylesheet">

<!-- Custom Fonts -->
<link href="{!! asset('public/assets/vendor/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">

<!-- Sweet alert -->
<link href="{!! asset('public/assets/css/sweet-alert.css') !!}" rel="stylesheet" type="text/css">