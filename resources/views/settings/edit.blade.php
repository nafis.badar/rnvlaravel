@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Update Settings</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Update Settings In RNV Software
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    {!! Form::open(['route' => ['settings.update'], 'method' => 'patch', 'id' => 'update-settings', 'role' => 'form']) !!}

                                        <div class="form-group">
                                            <label>Per Class Duration(Mins.)</label>
                                            <input class="form-control"  name="PER_CLASS_DURATION" value="{!! $settings1->config_value !!}">
                                        </div>

                                        <div class="form-group">
                                            <label>TOTAL_CLASS_PER_DAY</label>
                                            <input class="form-control"  name="TOTAL_CLASS_PER_DAY" value="{!! $settings2->config_value !!}">
                                        </div>
                                        
                                        <button type="submit" class="btn btn-default" id="update_btn">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>

@endsection

@section('js')
    @include('includes.js')
    <script type="text/javascript">

        $(document).ready(function(){
            $("form#update-settings").on('submit', function(){
                $("#update_btn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    method      : 'POST',
                    data        : formData,
                    url         : $(this).attr('action'),
                    processData : false, // Don't process the files
                    contentType : false, // Set content type to false as jQuery will tell the server its a query string request
                    dataType    : 'json',
                    success     : function(response){
                        if(response.success == true)
                        {
                            $("#update_btn").attr("disabled", false);
                            swal({   
                                    title: "Success",   
                                    text: response.data,   
                                    type: "success",   
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    timer: 3000
                            });
                            
                            location.reload(true);
                        }
                        else
                        {
                            $("#update_btn").attr("disabled", false);
                            $.notify(""+response.data+"", {type:"danger"});
                            
                        }
                    },
                    error       : function(data){
                        $("#update_btn").attr("disabled", false);
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors.errors, function(index, value) {
                            $.notify(""+value+"", {type:"danger"});
                        });
                    }

                });
                return false;
            });

        });
    </script>
@endsection
