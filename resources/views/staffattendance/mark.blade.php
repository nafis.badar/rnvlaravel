@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Staff Today's Attendances</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            RNV Staff Today's Attendances
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Staff Name</th>
                                        <th>Is Present</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if($staffattendances->count())
                                        <?php $odd_even=1; ?>
                                        @foreach($staffattendances as $staffattendance)
                                            <?php if($odd_even == 1){$class="odd"; $odd_even=2;}else{$class="even"; $odd_even=1;}  ?>
                                            <tr class="{!! $class !!}">
                                                <td>{!! $staffattendance->user->name !!}</td>
                                                <td>
                                                    <select class="form-control" id="attendance_{!!  $staffattendance->id !!}" onchange="changeAttendanceMark('{!!  $staffattendance->id !!}')">
                                                        <option value="Yes">Yes</option>
                                                        <option value="No" @if($staffattendance->is_present == 'No') selected @endif>No</option>
                                                    </select>
                                                </td>
                                                
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>

@endsection

@section('js')
    @include('includes.js')
    <script type="text/javascript">

        function changeAttendanceMark(attendance_id){
            var  is_present = $('#attendance_'+attendance_id).val();
            $.ajax({
            headers   :{'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            method      : 'POST',
            data       : {id: attendance_id, is_present: is_present},
            url         : "{{ route('staffattendance.mark.update') }}",
            //processData : false, // Don't process the files
            //contentType : false, // Set content type to false as jQuery will tell the server its a query string request
            dataType    : 'json',
            success     : function(response){
                if(response.success == true)
                {
                    
                }
                else
                {
                    $.notify(""+response.data+"", {type:"danger"});
                }
            },
            error       : function(data){
                
            }
    }); 
        }
    </script>
@endsection
