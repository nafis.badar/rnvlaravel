@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Staffs</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-9">
                </div>
                <div class="col-lg-3">
                        <h4><a href="{!! route('staff.create') !!}"><span class="glyphicon glyphicon-plus"> Add </span></a></h4>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            RNV Staffs
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Address</th>
                                        <th>Photo</th>
                                        <th>Subjects</th>
                                        <th>Shifts</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if($staffs->count())
                                        <?php $odd_even=1; ?>
                                        @foreach($staffs as $staff)
                                            <?php if($odd_even == 1){$class="odd"; $odd_even=2;}else{$class="even"; $odd_even=1;}  ?>
                                            <tr class="{!! $class !!}">
                                                <td>{!! $staff->name !!}</td>
                                                <td>{!! $staff->email !!}</td>
                                                <td>{!! $staff->mobile !!}</td>
                                                <td>{!! $staff->address !!}</td>
                                                <td>@if($staff->photo != null) <img src="{!! assset('/public/images/staff/'.$staff->photo) !!}" height="50px" width="50px"> @endif </td>
                                                <td>
                                                    @if($staff->subjects->count())
                                                        @foreach($staff->subjects as $subject)
                                                            {!! $subject->name !!} &nbsp;&nbsp;
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($staff->shifts->count())
                                                        @foreach($staff->shifts as $shift)
                                                            {!! $shift->name !!} &nbsp;&nbsp;
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td> <a href="{!! route('staff.edit',$staff->id) !!}"><span class="glyphicon glyphicon-pencil"></span> </a>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>

@endsection

@section('js')
    @include('includes.js')
@endsection
