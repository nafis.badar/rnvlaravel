@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">New Staff</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            New Staff In RNV Software
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    {!! Form::open(['route' => 'staff.store', 'method' => 'POST', 'id' => 'add-staff', 'role' => 'form']) !!}
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" placeholder="Enter Stafff Name" name="name">
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" placeholder="staff@gmail.com" name="email">
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="******" name="password">
                                        </div>

                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <input class="form-control" placeholder="1023045789" name="mobile">
                                        </div>

                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea name="address" class="form-control"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>Subjects Teach</label>
                                            {!! Form::select('subjects[]',$subjects, null, ['class' => 'form-control', 'multiple' => 'multiple']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label>Shifts</label>
                                            {!! Form::select('shifts[]',$shifts, null, ['class' => 'form-control', 'multiple' => 'multiple']) !!}
                                        </div>
                                        
                                        
                                        <button type="submit" class="btn btn-default" id="add_btn">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    {!!  Form::close() !!}
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>

@endsection

@section('js')
    @include('includes.js')

    <script type="text/javascript">

        $(document).ready(function(){
            $("form#add-staff").on('submit', function(){
                $("#add_btn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    method      : 'POST',
                    data        : formData,
                    url         : $(this).attr('action'),
                    processData : false, // Don't process the files
                    contentType : false, // Set content type to false as jQuery will tell the server its a query string request
                    dataType    : 'json',
                    success     : function(response){
                        if(response.success == true)
                        {
                            $("#add_btn").attr("disabled", false);
                            swal({   
                                    title: "Success",   
                                    text: response.data,   
                                    type: "success",   
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    timer: 3000
                            });
                            
                            var APP_URL = {!! json_encode(url('/')) !!};
                            APP_URL = APP_URL+'/staffs';
                            setTimeout(function(){ window.location.href=APP_URL; }, 2000);
                        }
                        else
                        {
                            $("#add_btn").attr("disabled", false);
                            $.notify(""+response.data+"", {type:"danger"});
                            
                        }
                    },
                    error       : function(data){
                        $("#add_btn").attr("disabled", false);
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors.errors, function(index, value) {
                            $.notify(""+value+"", {type:"danger"});
                        });
                    }

                });
                return false;
            });

        });
    </script>
@endsection
