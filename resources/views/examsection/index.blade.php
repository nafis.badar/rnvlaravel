@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Exam Sections</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-9">
                </div>
                <div class="col-lg-3">
                        <h4><a href="{!! route('examsection.create',$exam_id) !!}"><span class="glyphicon glyphicon-plus"> Add </span></a></h4>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            RNV Exam Sections
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Exam Name</th>
                                        <th>Class Name</th>
                                        <th>Subject Name</th>
                                        <th> Exam Date</th>
                                        <th>Is Time Dependent?</th>
                                        <th>Time(mins.)</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if($examsections->count())
                                        <?php $odd_even=1; ?>
                                        @foreach($examsections as $examsection)
                                            <?php if($odd_even == 1){$class="odd"; $odd_even=2;}else{$class="even"; $odd_even=1;}  ?>
                                            <tr class="{!! $class !!}">
                                                <td>{!! $examsection->exam->name !!}</td>
                                                <td>{!! $examsection->exam->classtable->name !!}</td>
                                                <td>{!! $examsection->subject->name !!}</td>
                                                <td>{!! $examsection->exam_date !!}</td>
                                                <td>{!! $examsection->is_time_dependent !!}</td>
                                                <td>{!! $examsection->duration !!}</td>
                                                
                                                <td> <a href="{!! route('examsection.edit',$examsection->id) !!}"><span class="glyphicon glyphicon-pencil"></span> </a>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <a href="{!! route('question.index',$examsection->id) !!}">Questions</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>

@endsection

@section('js')
    @include('includes.js')
@endsection
