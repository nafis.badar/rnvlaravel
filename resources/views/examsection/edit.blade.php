@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Update Exam Section</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Update Exam Section In RNV Software
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    {!! Form::open(['route' => ['examsection.update',$examsection->id], 'method' => 'patch', 'id' => 'update-examsection', 'role' => 'form']) !!}
                                        <input type="hidden" name="exam_id" value="{!! $examsection->exam_id !!}">
                                        <div class="form-group">
                                            <label>Subject</label>
                                            {!! Form::select('subject',$subjects,$examsection->subject_id,['class' => 'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label> Exam Date</label>
                                            <input type="date" class="form-control" placeholder="Enter Exam Date" name="exam_date" value="{!! $examsection->exam_date !!}">
                                        </div>


                                        <div class="form-group">
                                            <label> Is Time Dependent?</label>
                                            <select class="form-control" name="is_time_dependent">
                                                <option value="No">No</option>
                                                <option value="Yes" @if($examsection->is_time_dependent == 'Yes') selected @endif>Yes</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Duration(Mins.)</label>
                                            <input type="text" class="form-control" placeholder="Enter Duration" name="duration" value="{!! $examsection->duration !!}">
                                        </div>
                                        
                                        <button type="submit" class="btn btn-default" id="update_btn">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>

@endsection

@section('js')
    @include('includes.js')
    <script type="text/javascript">

        $(document).ready(function(){
            $("form#update-examsection").on('submit', function(){
                $("#update_btn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    method      : 'POST',
                    data        : formData,
                    url         : $(this).attr('action'),
                    processData : false, // Don't process the files
                    contentType : false, // Set content type to false as jQuery will tell the server its a query string request
                    dataType    : 'json',
                    success     : function(response){
                        if(response.success == true)
                        {
                            $("#update_btn").attr("disabled", false);
                            swal({   
                                    title: "Success",   
                                    text: response.data,   
                                    type: "success",   
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    timer: 3000
                            });
                            
                            var APP_URL = {!! json_encode(url('/')) !!};
                            APP_URL = APP_URL+'/exams';
                            setTimeout(function(){ window.location.href=APP_URL; }, 2000);
                        }
                        else
                        {
                            $("#update_btn").attr("disabled", false);
                            $.notify(""+response.data+"", {type:"danger"});
                            
                        }
                    },
                    error       : function(data){
                        $("#update_btn").attr("disabled", false);
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors.errors, function(index, value) {
                            $.notify(""+value+"", {type:"danger"});
                        });
                    }

                });
                return false;
            });

        });
    </script>
@endsection
