@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">New Question</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            New Question In RNV Software
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    {!! Form::open(['route' => 'question.store', 'method' => 'POST', 'id' => 'add-question', 'role' => 'form']) !!}
                                        <input type="hidden" name="examsection_id" value="{!! $examsection_id !!}">

                                        <div class="form-group">
                                            <label>Question No.</label>
                                            <input type="text" class="form-control" placeholder="Enter Question No." name="question_no">
                                        </div>

                                        <div class="form-group">
                                            <label>Question Text</label>
                                            <textarea class="form-control" name="question_text"></textarea>
                                        </div>


                                        <div class="form-group">
                                            <label> Type</label>
                                            <select class="form-control" name="type" onchange="getType()" id="type">
                                                <option value="Descriptive">Descriptive</option>
                                                <option value="MCQ">MCQ</option>
                                            </select>
                                        </div>

                                        <div class="form-group" id="no_of_opt_div" style="display: none;">
                                            <label>Enter Number Of Options</label>
                                            <input type="text" class="form-control" placeholder="No of options" name="no_of_options" id="no_of_options">
                                        </div>
                                        <div  id="opt_div" style="display: none;">

                                        </div>

                                        <div class="form-group">
                                            <label>Marks</label>
                                            <input type="text" class="form-control" placeholder=" Question Marks." name="mark">
                                        </div>

                                        <button type="submit" class="btn btn-default" id="add_btn">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    {!!  Form::close() !!}
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>

@endsection

@section('js')
    @include('includes.js')

    <script type="text/javascript">

        $(document).ready(function(){
            $("form#add-question").on('submit', function(){
                $("#add_btn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    method      : 'POST',
                    data        : formData,
                    url         : $(this).attr('action'),
                    processData : false, // Don't process the files
                    contentType : false, // Set content type to false as jQuery will tell the server its a query string request
                    dataType    : 'json',
                    success     : function(response){
                        if(response.success == true)
                        {
                            $("#add_btn").attr("disabled", false);
                            swal({   
                                    title: "Success",   
                                    text: response.data,   
                                    type: "success",   
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    timer: 3000
                            });
                            
                            var APP_URL = {!! json_encode(url('/')) !!};
                            APP_URL = APP_URL+'/exams';
                            setTimeout(function(){ window.location.href=APP_URL; }, 2000);
                        }
                        else
                        {
                            $("#add_btn").attr("disabled", false);
                            $.notify(""+response.data+"", {type:"danger"});
                            
                        }
                    },
                    error       : function(data){
                        $("#add_btn").attr("disabled", false);
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors.errors, function(index, value) {
                            $.notify(""+value+"", {type:"danger"});
                        });
                    }

                });
                return false;
            });

        });

        function getType(){
            var type = $('#type').val();
            if(type == 'MCQ')
            {
                $('#no_of_opt_div').show();
            }
            else{
                $('#no_of_opt_div').hide();
                $('#opt_div').hide();
            }
        }

        $(document).ready(function(){
            $('#no_of_options').on('change',function(){
                var no_of_options = $('#no_of_options').val();
                no_of_options = Number(no_of_options);
                if(no_of_options != '')
                {
                    if(no_of_options>0)
                    {
                        var str = '';
                        for(var i=1;i<=no_of_options;i++)
                        {
                            str += '<div class="form-group"><label>Option '+i+'</label><input type="text" class="form-control" placeholder="Enter Option Value" name="option_no_'+i+'">&nbsp;&nbsp;<select class="form-control" name="is_correct_'+i+'"><option value="No">Wrong</option><option value="Yes">Correct</option></select></div>';
                        }

                        $('#opt_div').html(str);
                        $('#opt_div').show();
                    }
                    else{
                        $('#opt_div').hide();
                    }
                }
                else{
                    $('#opt_div').hide();
                }
            }); 
        });
    </script>
@endsection
