@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Questions</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-9">
                </div>
                <div class="col-lg-3">
                        <h4><a href="{!! route('question.create',$examsection_id) !!}"><span class="glyphicon glyphicon-plus"> Add </span></a></h4>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            RNV Exam Questions
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Question No</th>
                                        <th>Question Text</th>
                                        <th>Type</th>
                                        <th>Mark</th>
                                        <th>Options</th>
                                        <th>Correct Answer</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if($questions->count())
                                        <?php $odd_even=1; ?>
                                        @foreach($questions as $question)
                                            <?php if($odd_even == 1){$class="odd"; $odd_even=2;}else{$class="even"; $odd_even=1;}  ?>
                                            <tr class="{!! $class !!}">
                                                <td>{!! $question->question_no !!}</td>
                                                <td>{!! $question->question_text !!}</td>
                                                <td>{!! $question->type !!}</td>
                                                <td>{!! $question->mark !!}</td>
                                                <td>
                                                    <?php $correct_answer = ''; ?>
                                                    @if($question->type == 'MCQ')
                                                        <?php  $i=1;  ?>
                                                        @if($question->questionoptions->count())
                                                            @foreach($question->questionoptions as $option)
                                                            Option {!! $i++ !!}. &nbsp {!! $option->option_value !!} <br>
                                                            @if($option->is_correct == 'Yes')
                                                                <?php $correct_answer = $option->option_value; ?>
                                                            @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>{!! $correct_answer !!}</td>
                                                
                                                <td> <button onclick="delete_question({!! $question->id !!})">Delete </button>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>

@endsection

@section('js')
    @include('includes.js')
    <script type="text/javascript">
        function delete_question(id){
            swal({
                title: "Do you want to delete this question?",
                text: "",
                type: "warning",
                showCancelButton: true,
                showConfirmButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                confirmButtonColor: "#FF0000"
            });

            $(".confirm").unbind().on('click', function(){
                $.ajax({
                    headers   :{'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                    method      : 'POST',
                    data        : {id: id},
                    url         : "{{ route('question.delete') }}",
                    //processData : false, // Don't process the files
                    //contentType : false, // Set content type to false as jQuery will tell the server its a query string request
                    dataType    : 'json',
                    success     : function(response){
                        if(response.success == true)
                        {
                            swal({
                                            title: "Success",
                                            text: response.data,
                                            type: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            timer: 1000
                                    });
                            location.reload(true);
                        }
                    },
                    error       : function(data){
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function(index, value) {
                                    $.notify(""+value+"", {type:"danger"});
                        });
                    }
                });
            });
        }
    </script>
@endsection
