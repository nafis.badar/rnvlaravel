@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Timeschedules</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-9">
                </div>
                <div class="col-lg-3">
                        <h4><a href="{!! route('timeschedule.create') !!}"><span class="glyphicon glyphicon-plus"> Add </span></a></h4>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            RNV Timeschedules
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if($timeschedules->count())
                                        <?php $odd_even=1; ?>
                                        @foreach($timeschedules as $timeschedule)
                                            <?php if($odd_even == 1){$class="odd"; $odd_even=2;}else{$class="even"; $odd_even=1;}  ?>
                                            <tr class="{!! $class !!}">
                                                <td>{!! $timeschedule->name !!}</td>
                                                <td>{!! $timeschedule->start_time !!}</td>
                                                <td>{!! $timeschedule->end_time !!}</td>
                                                
                                                <td> <a href="{!! route('timeschedule.edit',$timeschedule->id) !!}"><span class="glyphicon glyphicon-pencil"></span> </a>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>

@endsection

@section('js')
    @include('includes.js')
@endsection
