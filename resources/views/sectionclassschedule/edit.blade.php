@extends('layout.app')

@section('css')
    @include('includes.css')
@endsection

@section('content')

    <div id="wrapper">

        @include('includes.navbar')
        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Update Sectionschedule</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Update Sectionschedule In RNV Software
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    {!! Form::open(['route' => ['sectionschedule.update',$sectionclassschedule->id], 'method' => 'patch', 'id' => 'update-sectionschedule', 'role' => 'form']) !!}
                                        <div class="form-group">
                                            <label>Class</label>
                                            {!! Form::select('class',$classtables,$sectionclassschedule->classtable_id,['class' => 'form-control','placeholder' => 'Select Class', 'id' => 'class_id']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label>Section</label>
                                            {!! Form::select('section',$sections,$sectionclassschedule->section_id,['class' => 'form-control','placeholder' => 'Select Section', 'id' => 'section_id']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label>Section Shifts</label>
                                            {!! Form::select('shift',$shifts,$sectionclassschedule->shift_id,['class' => 'form-control','placeholder' => 'Select Section Shift', 'id' => 'shift_id']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label>Subject</label>
                                            {!! Form::select('subject',$subjects,$sectionclassschedule->subject_id,['class' => 'form-control','placeholder' => 'Select Subject', 'id' => 'subject_id']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label>Teacher</label>
                                            {!! Form::select('staff',$staffs,$sectionclassschedule->user_id,['class' => 'form-control','placeholder' => 'Select Teacher', 'id' => 'staff_id']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label>Time Schedule</label>
                                            {!! Form::select('timeschedule',$timeschedules,$sectionclassschedule->timeschedule_id,['class' => 'form-control','placeholder' => 'Select Time Schedule', 'id' => 'timeschedule_id']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label>Day</label>
                                            {!! Form::select('day',$days,$sectionclassschedule->day_id,['class' => 'form-control','placeholder' => 'Select Day', 'id' => 'day_id']) !!}
                                        </div>
                                        
                                        <button type="submit" class="btn btn-default" id="update_btn">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>

@endsection

@section('js')
    @include('includes.js')
    <script type="text/javascript">

        $(document).ready(function(){
            $("form#update-sectionschedule").on('submit', function(){
                $("#update_btn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    method      : 'POST',
                    data        : formData,
                    url         : $(this).attr('action'),
                    processData : false, // Don't process the files
                    contentType : false, // Set content type to false as jQuery will tell the server its a query string request
                    dataType    : 'json',
                    success     : function(response){
                        if(response.success == true)
                        {
                            $("#update_btn").attr("disabled", false);
                            swal({   
                                    title: "Success",   
                                    text: response.data,   
                                    type: "success",   
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    timer: 3000
                            });
                            
                            var APP_URL = {!! json_encode(url('/')) !!};
                            APP_URL = APP_URL+'/sectionschedules';
                            setTimeout(function(){ window.location.href=APP_URL; }, 2000);
                        }
                        else
                        {
                            $("#update_btn").attr("disabled", false);
                            $.notify(""+response.data+"", {type:"danger"});
                            
                        }
                    },
                    error       : function(data){
                        $("#update_btn").attr("disabled", false);
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors.errors, function(index, value) {
                            $.notify(""+value+"", {type:"danger"});
                        });
                    }

                });
                return false;
            });

            $('#class_id').on('change',function(){
                getSectionsByClass();
            });

            $('#section_id').on('change',function(){
                getShiftsBySection();
            });

            $('#subject_id').on('change',function(){
                getTeachersBySubjectAndShift();
            });

            $('#shift_id').on('change',function(){
                getTeachersBySubjectAndShift();
            });

        });

        function getSectionsByClass()
{
    var class_id = $('#class_id').val();

    $.ajax({
            headers   :{'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            method      : 'POST',
            url         : "{{ route('section.getSectionsByClass') }}",
            data        : {class_id: class_id},
            //processData : false, // Don't process the files
            //contentType : false, // Set content type to false as jQuery will tell the server its a query string request
            dataType    : 'json',
            success     : function(response){
                if(response.success == true)
                {
                    
                    $('#section_id').html(response.data);
                    getShiftsBySection();
                    
                }
                else
                {
                    $('#section_id').html(response.data);
                    getShiftsBySection();
                    //$.notify(""+response.data+"", {type:"danger"});
                }
            },
            error       : function(data){
                /*var errors = $.parseJSON(data.responseText);
                $.each(errors, function(index, value) {
                            $.notify(""+value+"", {type:"danger"});
                });*/
            }
    });  
    
}

function getShiftsBySection()
{
    var section_id = $('#section_id').val();

    $.ajax({
            headers   :{'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            method      : 'POST',
            url         : "{{ route('shift.getShiftsBySection') }}",
            data        : {section_id: section_id},
            //processData : false, // Don't process the files
            //contentType : false, // Set content type to false as jQuery will tell the server its a query string request
            dataType    : 'json',
            success     : function(response){
                if(response.success == true)
                {
                    
                    $('#shift_id').html(response.data);
                    getTeachersBySubjectAndShift();
                    
                }
                else
                {
                    $('#shift_id').html(response.data);
                    getTeachersBySubjectAndShift();
                    //$.notify(""+response.data+"", {type:"danger"});
                }
            },
            error       : function(data){
                /*var errors = $.parseJSON(data.responseText);
                $.each(errors, function(index, value) {
                            $.notify(""+value+"", {type:"danger"});
                });*/
            }
    });  
    
}

function getTeachersBySubjectAndShift()
{
    var subject_id = $('#subject_id').val();
    var shift_id = $('#shift_id').val();

    $.ajax({
            headers   :{'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            method      : 'POST',
            url         : "{{ route('staff.getTeachersBySubjectAndShift') }}",
            data        : {subject_id: subject_id,shift_id: shift_id},
            //processData : false, // Don't process the files
            //contentType : false, // Set content type to false as jQuery will tell the server its a query string request
            dataType    : 'json',
            success     : function(response){
                if(response.success == true)
                {
                    
                    $('#staff_id').html(response.data);
                    
                }
                else
                {
                    $('#staff_id').html(response.data);
                    //$.notify(""+response.data+"", {type:"danger"});
                }
            },
            error       : function(data){
                /*var errors = $.parseJSON(data.responseText);
                $.each(errors, function(index, value) {
                            $.notify(""+value+"", {type:"danger"});
                });*/
            }
    });  
    
}
    </script>
@endsection
