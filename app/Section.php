<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function classtable()
    {
        return $this->belongsTo('App\Classtable');
    }

    public function sectionshifts()
    {
        return $this->hasMany('App\Sectionshift');
    }
}
