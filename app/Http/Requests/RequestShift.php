<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Shift;

class RequestShift extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Only allow logged in users
        // return \Auth::check();
        // Allows all users in
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $shift = Shift::find($this->id);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'shift_name'     => 'required|max:255|unique:shifts,name',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'shift_name'     => 'required|max:255|unique:shifts,name,'.$shift->id,
                ];
            }
            default:break;
        }
    }

    public function wantsJson()
    {
        return true;
    }
}
