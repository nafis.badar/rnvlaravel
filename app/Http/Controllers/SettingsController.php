<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestShift;
use App\Shift;
use App\Sectionshift;
use App\Staffattendance;
use App\User;
use App\Exam;
use App\Examsection;
use App\Classtable;
use App\Subject;
use App\Question;
use App\Questionoption;
use App\Setting;

use Auth;
use Hash;

date_default_timezone_set("Asia/Kolkata");

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request){
        $settings1 = Setting::find(1);
        $settings2 = Setting::find(2);
        return view('settings.edit',compact('settings1','settings2'));
    }

    public function update(Request $request){
        $this->validate($request,[
            'PER_CLASS_DURATION' => 'required|numeric|max:150',
            'TOTAL_CLASS_PER_DAY' => 'required|numeric|max:120',
            ]);
        $PER_CLASS_DURATION = $request->get('PER_CLASS_DURATION');
        $TOTAL_CLASS_PER_DAY = $request->get('TOTAL_CLASS_PER_DAY');

        if($PER_CLASS_DURATION == '')
        {
            return response()->json(['success' => false, 'data' => 'PER CLASS DURATION is required']);
        }

        if($TOTAL_CLASS_PER_DAY == '')
        {
            return response()->json(['success' => false, 'data' => 'TOTAL CLASS PER DAY is required']);
        }

        $settings1 = Setting::find(1);
        $settings1->config_value = $PER_CLASS_DURATION;
        $settings1->update();

        $settings2 = Setting::find(2);
        $settings2->config_value = $TOTAL_CLASS_PER_DAY;
        $settings2->update();

        return response()->json(['success' => true, 'data' => 'Settings updated successfully']);
    }

    
}