<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestShift;
use App\Shift;
use App\Sectionshift;
use App\Staffattendance;
use App\User;
use App\Exam;
use App\Examsection;
use App\Classtable;
use App\Subject;

use Auth;
use Hash;

date_default_timezone_set("Asia/Kolkata");

class ExamsectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request,$exam_id){
        $examsections = Examsection::with('exam.classtable','subject')->where('exam_id',$exam_id)->orderBy('exam_date','DESC')->get();
        return view('examsection.index',compact('examsections','exam_id'));
    }

    public function create(Request $request,$exam_id){
        $subjects = Subject::pluck('name','id');
        return view('examsection.add',compact('subjects','exam_id'));
    }

    public function store(Request $request){
        $exam_id = $request->get('exam_id');
        $subject_id = $request->get('subject');
        $exam_date = $request->get('exam_date');
        $is_time_dependent = $request->get('is_time_dependent');
        $duration = $request->get('duration');

        if($subject_id == '')
        {
            return response()->json(['success' => false, 'data' => 'Please select a subject']);
        }

        if($exam_date == '')
        {
            return response()->json(['success' => false, 'data' => 'Exam date is required']);
        }

        if($is_time_dependent == 'Yes')
        {
            if($duration == '')
            {
                return response()->json(['success' => false, 'data' => 'Duration is required']);
            }
        }

        $exam_date = date('Y-m-d',strtotime($exam_date));

        if($exam_date<date('Y-m-d'))
        {
            return response()->json(['success' => false, 'data' => 'Exam can not be start today. Need Preparation Time.']);
        }

        $check = Examsection::where('exam_id',$exam_id)->where('exam_date',$exam_date)->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'One exam already added on this date']);
        }

        $examsection = new Examsection;
        $examsection->exam_id = $exam_id;
        $examsection->subject_id = $subject_id;
        $examsection->exam_date = $exam_date;
        $examsection->is_time_dependent = $is_time_dependent;
        $examsection->duration = $duration;
        $examsection->save();

        return response()->json(['success' => true, 'data' => 'Exam Section created successfully']);
    }
    
    public function edit(Request $request,$id){
        $examsection = Examsection::find($id);
        $subjects = Subject::pluck('name','id');
        return view('examsection.edit',compact('examsection','subjects'));
    }

    public function update(Request $request,$id){
        $exam_id = $request->get('exam_id');
        $subject_id = $request->get('subject');
        $exam_date = $request->get('exam_date');
        $is_time_dependent = $request->get('is_time_dependent');
        $duration = $request->get('duration');

        if($subject_id == '')
        {
            return response()->json(['success' => false, 'data' => 'Please select a subject']);
        }

        if($exam_date == '')
        {
            return response()->json(['success' => false, 'data' => 'Exam date is required']);
        }

        if($is_time_dependent == 'Yes')
        {
            if($duration == '')
            {
                return response()->json(['success' => false, 'data' => 'Duration is required']);
            }
        }

        $exam_date = date('Y-m-d',strtotime($exam_date));

        if($exam_date<date('Y-m-d'))
        {
            return response()->json(['success' => false, 'data' => 'Exam can not be start today. Need Preparation Time.']);
        }

        $check = Examsection::where('exam_id',$exam_id)->where('exam_date',$exam_date)->where('id','!=',$id)->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'One exam already added on this date']);
        }

        $examsection = Examsection::find($id);
        $examsection->subject_id = $subject_id;
        $examsection->exam_date = $exam_date;
        $examsection->is_time_dependent = $is_time_dependent;
        $examsection->duration = $duration;
        $examsection->update();

        return response()->json(['success' => true, 'data' => 'Exam Section updated successfully']);
    }
}