<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Subject;
use Auth;
use Hash;

class SubjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subject.add');
    }

    public function store(Request $request)
    {
        if($request->get('subject_name') == '')
        {
            return response()->json(['success' => false, 'data' => 'Subject Name is Required']);
        }

        $this->validate($request,[
            'subject_name' => 'required|alpha|unique:subjects,name',
            ]);
        $subject = new Subject;
        $subject->name = $request->get('subject_name');
        $subject->save();

        return response()->json(['success' => true, 'data' => 'Subject Created Successfully']);
    }

    public function index(Request $request)
    {
        $subjects = Subject::orderBy('id','DESC')->paginate(10);
        return view('subject.index',compact('subjects'));
    }

    public function edit(Request $request,$id)
    {
        $subject = Subject::where('id',$id)->first();
        return view('subject.edit',compact('subject'));
    }

    public function update(Request $request,$id)
    {
        if($request->get('subject_name') == '')
        {
            return response()->json(['success' => false, 'data' => 'Subject Name is Required']);
        }
        $this->validate($request,[
            'subject_name' => 'required|alpha|unique:subjects,name',
            ]);
        $subject = Subject::find($id);
        $subject->name = $request->get('subject_name');
        $subject->update();

        return response()->json(['success' => true, 'data' => 'Subject Updated Successfully']);
    }
}