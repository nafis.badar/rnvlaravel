<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestShift;
use App\Shift;
use App\Sectionshift;
use Auth;
use Hash;

class ShiftController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shift.add');
    }

    public function store(RequestShift $request)
    {
        $this->validate($request,[
            'shift_name' => 'required|alpha|unique:shifts,name',
            ]);
        $shift = new Shift;
        $shift->name = $request->get('shift_name');
        $shift->save();

        return response()->json(['success' => true, 'data' => 'Shift Created Successfully']);
    }

    public function index(Request $request)
    {
        $shifts = Shift::orderBy('id','DESC')->paginate(10);
        return view('shift.index',compact('shifts'));
    }

    public function edit(Request $request,$id)
    {
        $shift = Shift::where('id',$id)->first();
        return view('shift.edit',compact('shift'));
    }

    public function update(RequestShift $request,$id)
    {

        $this->validate($request,[
            'shift_name' => 'required|alpha|unique:shifts,name',
            ]);
        $shift = Shift::find($id);
        $shift->name = $request->get('shift_name');
        $shift->update();

        return response()->json(['success' => true, 'data' => 'Shift Updated Successfully']);
    }

    public function getShiftsBySection(Request $request){
        $section_id = $request->get('section_id');
        $sectionshifts = Sectionshift::where('section_id',$section_id)->get();
        $str = '';
        if($sectionshifts->count())
        {
            foreach($sectionshifts as $sectionshift)
            {
                $shift = Shift::find($sectionshift->shift_id);
                $str .= '<option value="'.$shift->id.'">'.$shift->name.'</option>';
            }

            return response()->json(['success' => true, 'data' => $str]);
        }
        else{
            $str = '<option>Please Add Shift For This Section</option>';
            return response()->json(['success' => false, 'data' => $str]);
        }
    }
}