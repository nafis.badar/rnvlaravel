<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestShift;
use App\Shift;
use App\Sectionshift;
use App\Staffattendance;
use App\User;
use App\Exam;
use App\Examsection;
use App\Classtable;
use App\Subject;
use App\Question;
use App\Questionoption;

use Auth;
use Hash;

date_default_timezone_set("Asia/Kolkata");

class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request,$examsection_id){
        $questions = Question::with('questionoptions')->where('examsection_id',$examsection_id)->orderBy('question_no','ASC')->get();
        return view('question.index',compact('questions','examsection_id'));
    }

    public function create(Request $request,$examsection_id){
        return view('question.add',compact('examsection_id'));
    }

    public function store(Request $request){
        $examsection_id = $request->get('examsection_id');
        $question_no = $request->get('question_no');
        $question_text = $request->get('question_text');
        $type = $request->get('type');
        $mark = $request->get('mark');

        if($question_no == '')
        {
            return response()->json(['success' => false, 'data' => 'Question no is required']);
        }

        if($question_text == '')
        {
            return response()->json(['success' => false, 'data' => 'Question text is required']);
        }

        if($type == 'MCQ')
        {
            $no_of_options = $request->get('no_of_options');
            if($no_of_options == '')
            {
                return response()->json(['success' => false, 'data' => 'Please add atleast 2 options']);
            }

            if($no_of_options < 2)
            {
                return response()->json(['success' => false, 'data' => 'Please add atleast 2 options']);
            }

            $is_correct = 'No';
            $correct_count = 0;
            for($i=1;$i<=$no_of_options;$i++)
            {
                $var = 'option_no_'.$i;
                if($request->get($var) == '')
                {
                    return response()->json(['success' => false, 'data' => 'Option can not be blank']);
                }

                $var = 'is_correct_'.$i;
                if($request->get($var) == 'Yes')
                {
                    $is_correct = 'Yes';
                    $correct_count++;
                }
            }

            if($is_correct == 'No' || $correct_count>1)
            {
                return response()->json(['success' => false, 'data' => 'One option should be correct']);
            }
        }

        if($mark == '')
        {
            return response()->json(['success' => false, 'data' => 'Mark is required']);
        }

        $check = Question::where('examsection_id',$examsection_id)->where('question_no',$question_no)->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'Question no already exists']);
        }

        $question = new Question;
        $question->examsection_id = $examsection_id;
        $question->question_no = $question_no;
        $question->question_text = $question_text;
        $question->type = $type;
        $question->mark = $mark;
        $question->save();

        if($type == 'MCQ')
        {
            for($i=1;$i<=$no_of_options;$i++)
            {
                $questionoption = new Questionoption;
                $questionoption->question_id = $question->id;
                $questionoption->type = 'Text';
                $var = 'option_no_'.$i;
                $questionoption->option_value = $request->get($var);
                $var = 'is_correct_'.$i;
                $questionoption->is_correct = $request->get($var);
                $questionoption->save();
            }
        }

        return response()->json(['success' => true, 'data' => 'Question created successfully']);
    }
    
    public function edit(Request $request,$id){
        $question = Question::find($id);
        return view('question.edit',compact('question'));
    }

    public function update(Request $request,$id){
        $examsection_id = $request->get('examsection_id');
        $question_no = $request->get('question_no');
        $question_text = $request->get('question_text');
        $type = $request->get('type');
        $mark = $request->get('mark');

        if($question_no == '')
        {
            return response()->json(['success' => false, 'data' => 'Question no is required']);
        }

        if($question_text == '')
        {
            return response()->json(['success' => false, 'data' => 'Question text is required']);
        }

        if($type == 'MCQ')
        {
            
        }

        if($mark == '')
        {
            return response()->json(['success' => false, 'data' => 'Mark is required']);
        }

        $check = Question::where('examsection_id',$examsection_id)->where('question_no',$question_no)->where('id','!=',$id)->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'Question no already exists']);
        }

        $question = Question::find($id);
        $question->examsection_id = $examsection_id;
        $question->question_no = $question_no;
        $question->question_text = $question_text;
        $question->type = $type;
        $question->mark = $mark;
        $question->update();

        return response()->json(['success' => true, 'data' => 'Question updated successfully']);
    }

    public function delete(Request $request){
        $id = $request->get('id');
        $questionoptions = Questionoption::where('question_id',$id)->get();
        if($questionoptions->count())
        {
            foreach($questionoptions as $option)
            {
                $option->delete();
            }
        }

        $question = Question::find($id);
        $question->delete();

        return response()->json(['success' => true, 'data' => 'Question deleted successfully']);
    }
}