<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Timeschedule;
use App\Sectionclassschedule;
use App\Classtable;
use App\Section;
use App\User;
use App\Day;
use App\Subject;
use App\Shift;
use App\Sectionshift;

use Auth;
use Hash;

class SectionclassscheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classtables = Classtable::pluck('name','id');
        $sections = Section::pluck('name','id');
        $staffs = User::where('role','staff')->pluck('name','id');
        $timeschedules = Timeschedule::pluck('name','id');
        $days = Day::pluck('name','id');
        $subjects = Subject::pluck('name','id');

        return view('sectionclassschedule.add',compact('classtables','sections','staffs','timeschedules','days','subjects'));
    }

    public function store(Request $request)
    {
        if($request->get('class') == '')
        {
            return response()->json(['success' => false, 'data' => 'Class is required']);
        }

        if($request->get('section') == '')
        {
            return response()->json(['success' => false, 'data' => 'Section is required']);
        }

        if($request->get('shift') == '')
        {
            return response()->json(['success' => false, 'data' => 'Shift is required']);
        }

        if($request->get('timeschedule') == '')
        {
            return response()->json(['success' => false, 'data' => 'Timeschedule is required']);
        }

        if($request->get('day') == '')
        {
            return response()->json(['success' => false, 'data' => 'Day is required']);
        }

        $check = Sectionclassschedule::where('classtable_id',$request->get('class'))->where('section_id',$request->get('section'))->where('shift_id',$request->get('shift'))->where('timeschedule_id',$request->get('timeschedule'))->where('day_id',$request->get('day'))->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'Class already added.']);
        }
        
        $check = Sectionclassschedule::where('classtable_id',$request->get('class'))->where('section_id',$request->get('section'))->where('shift_id',$request->get('shift'))->where('subject_id',$request->get('subject'))->where('day_id',$request->get('day'))->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'Subject already added on the given day.']);
        }

        if($request->get('staff') != '')
        {
            $check = Sectionclassschedule::where('shift_id',$request->get('shift'))->where('timeschedule_id',$request->get('timeschedule'))->where('day_id',$request->get('day'))->where('user_id',$request->get('staff'))->first();
            if($check != null)
            {
                return response()->json(['success' => false, 'data' => 'Teacher already have a class on that time.']);
            }
        }
        


        $sectionclassschedule = new Sectionclassschedule;
        $sectionclassschedule->classtable_id = $request->get('class');
        $sectionclassschedule->section_id = $request->get('section');
        $sectionclassschedule->shift_id = $request->get('shift');
        $sectionclassschedule->timeschedule_id = $request->get('timeschedule');
        $sectionclassschedule->day_id = $request->get('day');
        $sectionclassschedule->user_id = $request->get('staff');
        $sectionclassschedule->subject_id = $request->get('subject');
        $sectionclassschedule->save();

        return response()->json(['success' => true, 'data' => 'Sectionclassschedule Created Successfully']);
    }

    public function index(Request $request)
    {
        $sectionclassschedules = Sectionclassschedule::orderBy('classtable_id','ASC')->get();
        return view('sectionclassschedule.index',compact('sectionclassschedules'));
    }

    public function edit(Request $request,$id)
    {
        $sectionclassschedule = Sectionclassschedule::where('id',$id)->first();

        $classtables = Classtable::pluck('name','id');
        $sections = Section::where('classtable_id',$sectionclassschedule->classtable_id)->pluck('name','id');

        $subject_id = $sectionclassschedule->subject_id;
        $shift_id = $sectionclassschedule->shift_id;

        $teachers = User::whereHas('shifts',function($query) use ($shift_id){
            $query->where('shift_id',$shift_id);
        })->get();

        $arr = [];
        if($teachers->count())
        {
            $teachers = User::whereHas('subjects',function($query) use ($subject_id){
                $query->where('subject_id',$subject_id);
            })->get();

            if($teachers->count())
            {
                foreach($teachers as $teacher)
                {
                    array_push($arr,$teacher->id);
                }
            }
        }

        $staffs = User::where('role','staff')->whereIn('id',$arr)->pluck('name','id');
        $timeschedules = Timeschedule::pluck('name','id');
        $days = Day::pluck('name','id');
        $subjects = Subject::pluck('name','id');

        $sectionshifts = Sectionshift::where('section_id',$sectionclassschedule->section_id)->get();
        $arr = [];
        if($sectionshifts->count())
        {
            foreach($sectionshifts as $sectionshift)
            {
                array_push($arr,$sectionshift->shift_id);
            }
        }
        $shifts = Shift::whereIn('id',$arr)->pluck('name','id');

        

        return view('sectionclassschedule.edit',compact('sectionclassschedule','classtables','sections','staffs','timeschedules','days','timeschedules','subjects','shifts'));
    }

    public function update(Request $request,$id)
    {
        if($request->get('class') == '')
        {
            return response()->json(['success' => false, 'data' => 'Class is required']);
        }

        if($request->get('section') == '')
        {
            return response()->json(['success' => false, 'data' => 'Section is required']);
        }

        if($request->get('shift') == '')
        {
            return response()->json(['success' => false, 'data' => 'Shift is required']);
        }

        if($request->get('timeschedule') == '')
        {
            return response()->json(['success' => false, 'data' => 'Timeschedule is required']);
        }

        if($request->get('day') == '')
        {
            return response()->json(['success' => false, 'data' => 'Day is required']);
        }

        $check = Sectionclassschedule::where('classtable_id',$request->get('class'))->where('section_id',$request->get('section'))->where('shift_id',$request->get('shift'))->where('timeschedule_id',$request->get('timeschedule'))->where('day_id',$request->get('day'))->where('id','!=',$id)->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'Class already added.']);
        }
        
        $check = Sectionclassschedule::where('classtable_id',$request->get('class'))->where('section_id',$request->get('section'))->where('shift_id',$request->get('shift'))->where('subject_id',$request->get('subject'))->where('day_id',$request->get('day'))->where('id','!=',$id)->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'Subject already added on the given day.']);
        }

        if($request->get('staff') != '')
        {
            $check = Sectionclassschedule::where('shift_id',$request->get('shift'))->where('timeschedule_id',$request->get('timeschedule'))->where('day_id',$request->get('day'))->where('user_id',$request->get('staff'))->where('id','!=',$id)->first();
            if($check != null)
            {
                return response()->json(['success' => false, 'data' => 'Teacher already have a class on that time.']);
            }
        }


        $sectionclassschedule = Sectionclassschedule::find($id);
        $sectionclassschedule->classtable_id = $request->get('class');
        $sectionclassschedule->section_id = $request->get('section');
        $sectionclassschedule->shift_id = $request->get('shift');
        $sectionclassschedule->timeschedule_id = $request->get('timeschedule');
        $sectionclassschedule->day_id = $request->get('day');
        $sectionclassschedule->user_id = $request->get('staff');
        $sectionclassschedule->subject_id = $request->get('subject');
        $sectionclassschedule->update();

        return response()->json(['success' => true, 'data' => 'Sectionclassschedule Updated Successfully']);
    }
}