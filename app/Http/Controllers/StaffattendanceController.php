<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestShift;
use App\Shift;
use App\Sectionshift;
use App\Staffattendance;
use App\User;
use Auth;
use Hash;

date_default_timezone_set("Asia/Kolkata");

class StaffattendanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $staffattendances = Staffattendance::orderBy('id','DESC')->get();
        return view('staffattendance.index',compact('staffattendances'));
    }

    public function edit(Request $request,$id)
    {
        $staffattendance = Staffattendance::where('id',$id)->first();
        return view('staffattendance.edit',compact('staffattendance'));
    }

    public function update(Request $request,$id)
    {
        $staffattendance = Staffattendance::find($id);
        $staffattendance->is_present = $request->get('is_present');
        $staffattendance->update();

        return response()->json(['success' => true, 'data' => 'Staff attendance Updated Successfully']);
    }

    public function createAttendance(Request $request)
    {
        $staffattendance = Staffattendance::where('curr_date',date('Y-m-d'))->first();
        if($staffattendance != null)
        {
            $staffattendances = Staffattendance::with('user')->where('curr_date',date('Y-m-d'))->get();
            return view('staffattendance.mark',compact('staffattendances'));
        }
        else{
            $users = User::where('role','staff')->get();
            if($users->count())
            {
                foreach($users as $user)
                {
                    $staffattendance = new Staffattendance;
                    $staffattendance->user_id = $user->id;
                    $staffattendance->curr_date = date('Y-m-d');
                    $staffattendance->is_present = 'No';
                    $staffattendance->save();
                }
            }

            $staffattendances = Staffattendance::with('user')->where('curr_date',date('Y-m-d'))->get();
            return view('staffattendance.mark',compact('staffattendances'));
        }
    }

    public function updateTodaysAttendance(Request $request)
    {
        $id =  $request->get('id');
        $is_present = $request->get('is_present');
        $staffattendance = Staffattendance::where('id',$id)->first();
        $staffattendance->is_present = $is_present;
        $staffattendance->update();

        return response()->json(['success' => true, 'data' => 'Staff attendance updated successfully']);
    }
}