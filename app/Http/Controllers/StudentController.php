<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestStudent;
use App\User;
use Auth;
use Hash;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.add');
    }

    public function store(RequestStudent $request)
    {
        $this->validate($request,[
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            'mobile' => 'nullable|min:10|max:11|numeric',
            ]);

        $student = new User;
        $student->name = $request->get('name');
        $student->email = $request->get('email');
        $student->password = Hash::make($request->get('password'));
        $student->mobile = $request->get('mobile');
        $student->address = $request->get('address');
        $student->role = 'student';
        $student->save();

        return response()->json(['success' => true, 'data' => 'Student Created Successfully']);
    }

    public function index(Request $request)
    {
        $students = User::where('role','student')->orderBy('id','DESC')->paginate(10);
        return view('student.index',compact('students'));
    }

    public function edit(Request $request,$id)
    {
        $student = User::where('role','student')->where('id',$id)->first();
        return view('student.edit',compact('student'));
    }

    public function update(RequestStudent $request,$id)
    {
        $this->validate($request,[
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            'mobile' => 'nullable|min:10|max:11|numeric',
            ]);
        $student = User::find($id);
        $student->name = $request->get('name');
        $student->mobile = $request->get('mobile');
        $student->address = $request->get('address');
        $student->update();

        return response()->json(['success' => true, 'data' => 'Student Updated Successfully']);
    }
}