<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Section;
use App\Classtable;
use App\Shift;
use App\Sectionshift;
use Auth;
use Hash;

class SectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes = Classtable::pluck('name','id');
        $shifts = Shift::pluck('name','id');
        return view('section.add',compact('classes','shifts'));
    }

    public function store(Request $request)
    {
        if($request->get('class_id') == '')
        {
            return response()->json(['success' => false, 'data' => 'Please Select a Class']);
        }

        if($request->get('section_name') == '')
        {
            return response()->json(['success' => false, 'data' => 'Section Name Is Required']);
        }

        $check = Section::where('classtable_id',$request->get('class_id'))->where('name',$request->get('section_name'))->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'Data already exists']);
        }
        $this->validate($request,[
            'section_name' => 'required|alpha_dash|max:120',
            ]);
        $section = new Section;
        $section->classtable_id = $request->get('class_id');
        $section->name = $request->get('section_name');
        $section->save();

        if($request->get('shifts'))
        {
            if(count($request->get('shifts')))
            {
                $shifts = $request->get('shifts');
                foreach($shifts as $shift)
                {
                    $sectionshift = new Sectionshift;
                    $sectionshift->shift_id = $shift;
                    $sectionshift->section_id = $section->id;
                    $sectionshift->save();
                }
            }
        }

        return response()->json(['success' => true, 'data' => 'Section Created Successfully']);
    }

    public function index(Request $request)
    {
        $sections = Section::with('classtable')->orderBy('id','DESC')->paginate(10);
        return view('section.index',compact('sections'));
    }

    public function edit(Request $request,$id)
    {
        $section = Section::with('sectionshifts')->where('id',$id)->first();
        $classes = Classtable::pluck('name','id');
        $shifts = Shift::pluck('name','id');

        $selected_shifts = [];
        if($section->sectionshifts->count())
        {
            foreach($section->sectionshifts as $sectionshift)
            {
                array_push($selected_shifts,$sectionshift->shift_id);
            }
        }
        return view('section.edit',compact('section','classes','shifts','selected_shifts'));
    }

    public function update(Request $request,$id)
    {
        if($request->get('class_id') == '')
        {
            return response()->json(['success' => false, 'data' => 'Please Select a Class']);
        }

        if($request->get('section_name') == '')
        {
            return response()->json(['success' => false, 'data' => 'Section Name Is Required']);
        }
        
        $check = Section::where('classtable_id',$request->get('class_id'))->where('name',$request->get('section_name'))->where('id','!=',$id)->first();
        if($check != null)
        {
            return response()->json(['success' => false, 'data' => 'Data already exists']);
        }

        $this->validate($request,[
            'section_name' => 'required|alpha_dash|max:120',
            ]);
        $section = Section::find($id);
        $section->classtable_id = $request->get('class_id');
        $section->name = $request->get('section_name');
        $section->update();

        if($request->get('shifts'))
        {
            if(count($request->get('shifts')))
            {
                $selected_shifts = [];
                $shifts = $request->get('shifts');
                foreach($shifts as $shift)
                {
                    $check = Sectionshift::where('section_id',$id)->where('shift_id',$shift)->first();
                    if($check == null)
                    {
                        $sectionshift = new Sectionshift;
                        $sectionshift->shift_id = $shift;
                        $sectionshift->section_id = $section->id;
                        $sectionshift->save();
                    }
                    array_push($selected_shifts,$shift);
                }

                $sectionshifts = Sectionshift::where('section_id',$id)->whereNotIn('shift_id',$selected_shifts)->get();
                if($sectionshifts->count())
                {
                    foreach($sectionshifts as $sectionshift)
                    {
                        $obj = Sectionshift::find($sectionshift->id);
                        $obj->delete();
                    } 
                }
            }
            else{
                $sectionshifts = Sectionshift::where('section_id',$id)->get();
                if($sectionshifts->count())
                {
                    foreach($sectionshifts as $sectionshift)
                    {
                        $obj = Sectionshift::find($sectionshift->id);
                        $obj->delete();
                    } 
                }
            }
        }
        else{
            $sectionshifts = Sectionshift::where('section_id',$id)->get();
            if($sectionshifts->count())
            {
                foreach($sectionshifts as $sectionshift)
                {
                    $obj = Sectionshift::find($sectionshift->id);
                    $obj->delete();
                } 
            }
        }

        return response()->json(['success' => true, 'data' => 'Section Updated Successfully']);
    }

    public function getSectionsByClass(Request $request){
        $class_id = $request->get('class_id');
        $sections = Section::where('classtable_id',$class_id)->get();
        $str = '';
        if($sections->count())
        {
            foreach($sections as $section)
            {
                $str .= '<option value="'.$section->id.'">'.$section->name.'</option>';
            }

            return response()->json(['success' => true, 'data' => $str]);
        }
        else{
            $str = '<option value="">No Section Added(Please Add)</option>';

            return response()->json(['success' => false, 'data' => $str]);
        }
    }
}