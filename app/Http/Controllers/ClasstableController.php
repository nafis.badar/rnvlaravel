<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestClasstable;
use App\Classtable;
use Auth;
use Hash;

class ClasstableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classtable.add');
    }

    public function store(RequestClasstable $request)
    {
        $this->validate($request,[
            'class_name' => 'required|alpha_dash|max:120',
            ]);
        $class = new Classtable;
        $class->name = $request->get('class_name');
        $class->save();

        return response()->json(['success' => true, 'data' => 'Class Created Successfully']);
    }

    public function index(Request $request)
    {
        $classes = Classtable::orderBy('id','DESC')->paginate(10);
        return view('classtable.index',compact('classes'));
    }

    public function edit(Request $request,$id)
    {
        $class = Classtable::where('id',$id)->first();
        return view('classtable.edit',compact('class'));
    }

    public function update(RequestClasstable $request,$id)
    {
        $this->validate($request,[
            'class_name' => 'required|alpha_dash|max:120',
            ]);
        $class = Classtable::find($id);
        $class->name = $request->get('class_name');
        $class->update();

        return response()->json(['success' => true, 'data' => 'Class Updated Successfully']);
    }
}