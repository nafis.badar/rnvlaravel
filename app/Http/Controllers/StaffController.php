<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestStaff;
use App\User;
use App\Subject;
use App\Shift;
use Auth;
use Hash;

class StaffController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::pluck('name','id');
        $shifts = Shift::pluck('name','id');
        return view('staff.add',compact('subjects','shifts'));
    }

    public function store(RequestStaff $request)
    {
        $this->validate($request,[
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            'mobile' => 'nullable|min:10|max:11|numeric',
            'address'=>'required|max:120',
            ]);
        $staff = new User;
        $staff->name = $request->get('name');
        $staff->email = $request->get('email');
        $staff->password = Hash::make($request->get('password'));
        $staff->mobile = $request->get('mobile');
        $staff->address = $request->get('address');
        $staff->role = 'staff';
        $staff->save();

        if($request->get('subjects'))
        {
            if(count($request->get('subjects')))
            {
                foreach($request->get('subjects') as $subject)
                {
                    $staff->subjects()->attach($subject);
                }
            }
        }

        if($request->get('shifts'))
        {
            if(count($request->get('shifts')))
            {
                foreach($request->get('shifts') as $shift)
                {
                    $staff->shifts()->attach($shift);
                }
            }
        }
        

        return response()->json(['success' => true, 'data' => 'Staff Created Successfully']);
    }

    public function index(Request $request)
    {
        $staffs = User::with('subjects','shifts')->where('role','staff')->orderBy('id','DESC')->paginate(10);
        return view('staff.index',compact('staffs'));
    }

    public function edit(Request $request,$id)
    {
        $staff = User::with('subjects','shifts')->where('role','staff')->where('id',$id)->first();
        $subjects = Subject::pluck('name','id');
        $selected_subjects=[];
        if($staff->subjects->count())
        {
            foreach($staff->subjects as $sub)
            {
                array_push($selected_subjects,$sub->id);
            }
        }

        $shifts = Shift::pluck('name','id');
        $selected_shifts=[];
        if($staff->shifts->count())
        {
            foreach($staff->shifts as $shift)
            {
                array_push($selected_shifts,$shift->id);
            }
        }

        return view('staff.edit',compact('staff','subjects','selected_subjects','shifts','selected_shifts'));
    }

    public function update(RequestStaff $request,$id)
    {
        $this->validate($request,[
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            'mobile' => 'nullable|min:10|max:11|numeric',
            'address'=>'required|max:120',
            ]);
        $staff = User::find($id);
        $staff->name = $request->get('name');
        $staff->mobile = $request->get('mobile');
        $staff->address = $request->get('address');
        $staff->update();

        $staff->subjects()->detach();
        if($request->get('subjects'))
        {
            if(count($request->get('subjects')))
            {
                foreach($request->get('subjects') as $subject)
                {
                    $staff->subjects()->attach($subject);
                }
            }
        }

        $staff->shifts()->detach();
        if($request->get('shifts'))
        {
            if(count($request->get('shifts')))
            {
                foreach($request->get('shifts') as $shift)
                {
                    $staff->shifts()->attach($shift);
                }
            }
        }
        

        return response()->json(['success' => true, 'data' => 'Staff Updated Successfully']);
    }

    public function getTeachersBySubjectAndShift(Request $request){
        $subject_id = $request->get('subject_id');
        $shift_id = $request->get('shift_id');

        $teachers = User::whereHas('shifts',function($query) use ($shift_id){
            $query->where('shift_id',$shift_id);
        })->get();

        $str = '';
        if($teachers->count())
        {
            $teachers = User::whereHas('subjects',function($query) use ($subject_id){
                $query->where('subject_id',$subject_id);
            })->get();

            if($teachers->count())
            {
                foreach($teachers as $teacher)
                {
                    $str .= '<option value="'.$teacher->id.'">'.$teacher->name.'</option>';
                    return response()->json(['success' => true, 'data' => $str]);
                }
            }
            else{
                $str = '<option value="">Please Add Teacher For This Subject</option>';
                return response()->json(['success' => false, 'data' => $str]);
            }
        }
        else{
            $str = '<option value="">Please Add Teacher In This Shift</option>';
            return response()->json(['success' => false, 'data' => $str]);
        }
    }
}