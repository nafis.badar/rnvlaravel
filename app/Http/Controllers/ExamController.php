<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestShift;
use App\Shift;
use App\Sectionshift;
use App\Staffattendance;
use App\User;
use App\Exam;
use App\Classtable;

use Auth;
use Hash;

date_default_timezone_set("Asia/Kolkata");

class ExamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){
        $exams = Exam::with('classtable')->orderBy('exam_start_date','DESC')->get();
        return view('exam.index',compact('exams'));
    }

    public function create(Request $request){
        $classtables = Classtable::pluck('name','id');
        return view('exam.add',compact('classtables'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required|alpha_dash|max:120',
            'start_date' => 'required|date',
            'end_date'    =>  'required|date|after_or_equal:start_date'
            ]);
        $name = $request->get('name');
        $class_id = $request->get('class');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        if($name == '')
        {
            return response()->json(['success' => false, 'data' => 'Name is required']);
        }

        if($class_id == '')
        {
            return response()->json(['success' => false, 'data' => 'Class is required']);
        }

        if($start_date == '')
        {
            return response()->json(['success' => false, 'data' => 'Exam start date is required']);
        }

        if($end_date == '')
        {
            return response()->json(['success' => false, 'data' => 'Exam end date is required']);
        }

        $start_date = date('Y-m-d',strtotime($start_date));
        $end_date = date('Y-m-d',strtotime($end_date));

        if($start_date<date('Y-m-d'))
        {
            return response()->json(['success' => false, 'data' => 'Exam can not be start today. Need Preparation Time.']);
        }

        if($start_date>$end_date)
        {
            return response()->json(['success' => false, 'data' => 'End date should be greater than start date']);
        }

        $exam = new Exam;
        $exam->name = $name;
        $exam->classtable_id = $class_id;
        $exam->exam_start_date = $start_date;
        $exam->exam_end_date = $end_date;
        $exam->save();

        return response()->json(['success' => true, 'data' => 'Exam created successfully']);
    }
    
    public function edit(Request $request,$id){
        $exam = Exam::find($id);
        $classtables = Classtable::pluck('name','id');
        return view('exam.edit',compact('exam','classtables'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'name' => 'required|alpha_dash|max:120',
            'start_date' => 'required|date',
            'end_date'    =>  'required|date|after_or_equal:start_date'
            ]);
        $name = $request->get('name');
        $class_id = $request->get('class');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        if($name == '')
        {
            return response()->json(['success' => false, 'data' => 'Name is required']);
        }

        if($class_id == '')
        {
            return response()->json(['success' => false, 'data' => 'Class is required']);
        }

        if($start_date == '')
        {
            return response()->json(['success' => false, 'data' => 'Exam start date is required']);
        }

        if($end_date == '')
        {
            return response()->json(['success' => false, 'data' => 'Exam end date is required']);
        }

        $start_date = date('Y-m-d',strtotime($start_date));
        $end_date = date('Y-m-d',strtotime($end_date));

        if($start_date<date('Y-m-d'))
        {
            return response()->json(['success' => false, 'data' => 'Exam can not be start today. Need Preparation Time.']);
        }

        if($start_date>$end_date)
        {
            return response()->json(['success' => false, 'data' => 'End date should be greater than start date']);
        }

        $exam = Exam::find($id);
        $exam->name = $name;
        $exam->classtable_id = $class_id;
        $exam->exam_start_date = $start_date;
        $exam->exam_end_date = $end_date;
        $exam->update();

        return response()->json(['success' => true, 'data' => 'Exam updated successfully']);
    }
}