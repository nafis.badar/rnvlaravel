<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RequestShift;
use App\Timeschedule;
use App\Setting;
use Auth;
use Hash;

class TimescheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('timeschedule.add');
    }

    public function store(Request $request)
    {
        if($request->get('name') == '')
        {
            return response()->json(['success' => false, 'data' => 'Name is required']);
        }

        if($request->get('start_time') == '')
        {
            return response()->json(['success' => false, 'data' => 'Start Time is required']);
        }

        $setting_1 = Setting::find(1);

        $end_time = date('H:i',strtotime("+".$setting_1->config_value." minutes",strtotime($request->get('start_time'))));


        $setting = Setting::find(2);
        $totaltimeschedules = Timeschedule::all();
        if($totaltimeschedules->count()>=$setting->config_value)
        {
            return response()->json(['success' => false, 'data' => 'You can add maximum '.$setting->config_value.' Timeschedule']);
        }
        $this->validate($request,[
            'name' => 'required|alpha_dash|max:120|unique:timeschedules,name'
            ]);
        $timeschedule = new Timeschedule;
        $timeschedule->name = $request->get('name');
        $timeschedule->start_time = $request->get('start_time');
        $timeschedule->end_time = $end_time;
        $timeschedule->save();

        return response()->json(['success' => true, 'data' => 'Timeschedule Created Successfully']);
    }

    public function index(Request $request)
    {
        $timeschedules = Timeschedule::orderBy('start_time','ASC')->paginate(10);
        return view('timeschedule.index',compact('timeschedules'));
    }

    public function edit(Request $request,$id)
    {
        $timeschedule = Timeschedule::where('id',$id)->first();
        return view('timeschedule.edit',compact('timeschedule'));
    }

    public function update(Request $request,$id)
    {
        if($request->get('name') == '')
        {
            return response()->json(['success' => false, 'data' => 'Name is required']);
        }

        if($request->get('start_time') == '')
        {
            return response()->json(['success' => false, 'data' => 'Start Time is required']);
        }

        $setting_1 = Setting::find(1);

        $end_time = date('H:i',strtotime("+".$setting_1->config_value." minutes",strtotime($request->get('start_time'))));
        $this->validate($request,[
            'name' => 'required|alpha_dash|max:120|unique:timeschedules,name'
            ]);


        $timeschedule = Timeschedule::find($id);
        $timeschedule->name = $request->get('name');
        $timeschedule->start_time = $request->get('start_time');
        $timeschedule->end_time = $end_time;
        $timeschedule->update();

        return response()->json(['success' => true, 'data' => 'Timeschedule Updated Successfully']);
    }
}