<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function examsection()
    {
        return $this->belongsTo('App\Examsection');
    }

    public function questionoptions(){
    	return $this->hasMany('App\Questionoption');
    }
}
