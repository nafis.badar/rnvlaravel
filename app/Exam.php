<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function classtable()
    {
        return $this->belongsTo('App\Classtable');
    }
}
