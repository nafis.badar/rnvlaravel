<?php

namespace App\Helpers;

//use Nestable\NestableTrait;
use App\Section;
use App\Shifts;
use App\Sectionshift;
use App\User;
use App\Classtable;
use App\Subject;
use App\Setting;

use DB;
use Response;
use URL;

use Auth;
class CustomHelper {
	public static function getShiftNamesForSection($section_id){
		$sectionshifts =  Sectionshift::with('shift')->where('section_id',$section_id)->get();
		$shiftnames = [];
		if($sectionshifts->count())
		{
			foreach($sectionshifts as $sectionshift)
			{
				array_push($shiftnames,$sectionshift->shift->name);
			}
		}
		$shiftnamesstr = implode(',',$shiftnames);
		return $shiftnamesstr;
	}

	public static function totalStudents(){
		$users = User::where('role','student')->count();
		return $users;
	}

	public static function totalStaffs(){
		$users = User::where('role','staff')->count();
		return $users;
	}

	public static function totalClass(){
		$classes = Classtable::count();
		return $classes;
	}

	public static function totalSubjects(){
		$subjects = Subject::count();
		return $subjects;
	}

	public static function settings($id){
		$setting = Setting::find($id);
		return $setting;
	}
}

?>