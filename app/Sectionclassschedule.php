<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sectionclassschedule extends Model
{

   public function classtable(){
   		return $this->belongsTo('App\Classtable');
   }

   public function section(){
   		return $this->belongsTo('App\Section');
   }

   public function shift(){
   		return $this->belongsTo('App\Shift');
   }

   public function subject(){
   		return $this->belongsTo('App\Subject');
   }

   public function user(){
   		return $this->belongsTo('App\User');
   }

   public function day(){
   		return $this->belongsTo('App\Day');
   }

   public function timeschedule(){
   		return $this->belongsTo('App\Timeschedule');
   }
}
