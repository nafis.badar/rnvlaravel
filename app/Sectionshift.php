<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sectionshift extends Model
{
    public function shift()
    {
        return $this->belongsTo('App\Shift');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }
}
