<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// students
Route::get('/students', 'StudentController@index')->name('student.index');
Route::get('/student/create', 'StudentController@create')->name('student.create');
Route::post('/student/save', 'StudentController@store')->name('student.store');
Route::get('/student/{id}/edit', 'StudentController@edit')->name('student.edit');
Route::patch('/student/{id}/update', 'StudentController@update')->name('student.update');

// staffs
Route::get('/staffs', 'StaffController@index')->name('staff.index');
Route::get('/staff/create', 'StaffController@create')->name('staff.create');
Route::post('/staff/save', 'StaffController@store')->name('staff.store');
Route::get('/staff/{id}/edit', 'StaffController@edit')->name('staff.edit');
Route::patch('/staff/{id}/update', 'StaffController@update')->name('staff.update');

// shifts
Route::get('/shifts', 'ShiftController@index')->name('shift.index');
Route::get('/shift/create', 'ShiftController@create')->name('shift.create');
Route::post('/shift/save', 'ShiftController@store')->name('shift.store');
Route::get('/shift/{id}/edit', 'ShiftController@edit')->name('shift.edit');
Route::patch('/shift/{id}/update', 'ShiftController@update')->name('shift.update');

// classes
Route::get('/classes', 'ClasstableController@index')->name('classtable.index');
Route::get('/class/create', 'ClasstableController@create')->name('classtable.create');
Route::post('/class/save', 'ClasstableController@store')->name('classtable.store');
Route::get('/class/{id}/edit', 'ClasstableController@edit')->name('classtable.edit');
Route::patch('/class/{id}/update', 'ClasstableController@update')->name('classtable.update');

// sections
Route::get('/sections', 'SectionController@index')->name('section.index');
Route::get('/section/create', 'SectionController@create')->name('section.create');
Route::post('/section/save', 'SectionController@store')->name('section.store');
Route::get('/section/{id}/edit', 'SectionController@edit')->name('section.edit');
Route::patch('/section/{id}/update', 'SectionController@update')->name('section.update');

// subjects
Route::get('/subjects', 'SubjectController@index')->name('subject.index');
Route::get('/subject/create', 'SubjectController@create')->name('subject.create');
Route::post('/subject/save', 'SubjectController@store')->name('subject.store');
Route::get('/subject/{id}/edit', 'SubjectController@edit')->name('subject.edit');
Route::patch('/subject/{id}/update', 'SubjectController@update')->name('subject.update');

// timeschedules
Route::get('/timeschedules', 'TimescheduleController@index')->name('timeschedule.index');
Route::get('/timeschedule/create', 'TimescheduleController@create')->name('timeschedule.create');
Route::post('/timeschedule/save', 'TimescheduleController@store')->name('timeschedule.store');
Route::get('/timeschedule/{id}/edit', 'TimescheduleController@edit')->name('timeschedule.edit');
Route::patch('/timeschedule/{id}/update', 'TimescheduleController@update')->name('timeschedule.update');

// sectionclassschedules
Route::get('/sectionschedules', 'SectionclassscheduleController@index')->name('sectionschedule.index');
Route::get('/sectionschedule/create', 'SectionclassscheduleController@create')->name('sectionschedule.create');
Route::post('/sectionschedule/save', 'SectionclassscheduleController@store')->name('sectionschedule.store');
Route::get('/sectionschedule/{id}/edit', 'SectionclassscheduleController@edit')->name('sectionschedule.edit');
Route::patch('/sectionschedule/{id}/update', 'SectionclassscheduleController@update')->name('sectionschedule.update');

Route::post('/get-sections-by-class', 'SectionController@getSectionsByClass')->name('section.getSectionsByClass');
Route::post('/get-shift-by-section', 'ShiftController@getShiftsBySection')->name('shift.getShiftsBySection');
Route::post('/get-teachers-by-shift-and-subject', 'StaffController@getTeachersBySubjectAndShift')->name('staff.getTeachersBySubjectAndShift');

// staff atttendances
Route::get('/staff-attendances', 'StaffattendanceController@index')->name('staffattendance.index');
Route::get('/staff-attendance/create', 'StaffattendanceController@createAttendance')->name('staffattendance.create');
Route::post('/update-mark-staff-attendance', 'StaffattendanceController@updateTodaysAttendance')->name('staffattendance.mark.update');
Route::get('/staff-attendance/{id}/edit', 'StaffattendanceController@edit')->name('staffattendance.edit');
Route::post('/staff-attendance/{id}/update', 'StaffattendanceController@update')->name('staffattendance.update');

// exams
Route::get('/exams', 'ExamController@index')->name('exam.index');
Route::get('/exam/create', 'ExamController@create')->name('exam.create');
Route::post('/exam/save', 'ExamController@store')->name('exam.store');
Route::get('/exam/{id}/edit', 'ExamController@edit')->name('exam.edit');
Route::patch('/exam/{id}/update', 'ExamController@update')->name('exam.update');

// examsections
Route::get('/exam/{exam_id}/examsections', 'ExamsectionController@index')->name('examsection.index');
Route::get('/exam/{exam_id}/examsection/create', 'ExamsectionController@create')->name('examsection.create');
Route::post('/examsection/save', 'ExamsectionController@store')->name('examsection.store');
Route::get('/examsection/{id}/edit', 'ExamsectionController@edit')->name('examsection.edit');
Route::patch('/examsection/{id}/update', 'ExamsectionController@update')->name('examsection.update');

// questions
Route::get('/examsection/{examsection_id}/questions', 'QuestionController@index')->name('question.index');
Route::get('/examsection/{examsection_id}/question/create', 'QuestionController@create')->name('question.create');
Route::post('/question/save', 'QuestionController@store')->name('question.store');
Route::post('/question/delete', 'QuestionController@delete')->name('question.delete');
Route::get('/question/{id}/edit', 'QuestionController@edit')->name('question.edit');
Route::patch('/question/{id}/update', 'QuestionController@update')->name('question.update');

// settings
Route::get('/settings', 'SettingsController@edit')->name('settings.edit');
Route::patch('/settings/update', 'SettingsController@update')->name('settings.update');
